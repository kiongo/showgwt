package com.showgwt.view.composite;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

public class UISaludarComposite extends Composite implements ClickHandler{
	private FlowPanel pnlContenedor;
	private Label lblNombre;
	private TextBox txtNombre;
	private Button btnSaludar;
	
	public UISaludarComposite(){
		initComponents();
		widgetListener();
	}
	
	private void initComponents(){
		pnlContenedor=new FlowPanel();
		lblNombre=new Label("Quien eres");
		txtNombre=new TextBox();
		btnSaludar=new Button("Saludar");
		pnlContenedor.add(lblNombre);
		pnlContenedor.add(txtNombre);
		pnlContenedor.add(btnSaludar);
		this.initWidget(pnlContenedor);
	}
	
	private void widgetListener(){
		btnSaludar.addClickHandler(this);
	}
	
	private void saludar(){
		Window.alert("Hola composite "+txtNombre.getText());
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(btnSaludar)){
			saludar();
		}
	}
	
}
