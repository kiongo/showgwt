package com.showgwt.view.uicssresource;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.showgwt.client.resource.MyResource;

public class UISaludar extends Composite{
	private FlowPanel pnlContenedor;
	private FlowPanel pnl1;
	private FlowPanel pnl2;
	private FlowPanel pnl3;
	private Label lblTitulo;
	private TextBox txtNombre;
	private Button btnSaludar;
	
	public UISaludar(){
		initComponents();
		style();
	}
	
	private void initComponents(){
		pnlContenedor=new FlowPanel();
		pnl1=new FlowPanel();
		pnl2=new FlowPanel();
		pnl3=new FlowPanel();
		lblTitulo=new Label("Quien eres: ");
		txtNombre=new TextBox();
		btnSaludar=new Button("Saludar");
		pnl1.add(lblTitulo);
		pnl2.add(txtNombre);
		pnl3.add(btnSaludar);
		pnlContenedor.add(pnl1);
		pnlContenedor.add(pnl2);
		pnlContenedor.add(pnl3);
		initWidget(pnlContenedor);
	}
	
	private void style(){
		MyResource.myResource.getUISaludarCss().ensureInjected();
		pnl1.addStyleName(MyResource.myResource.getUISaludarCss().pnl());
		pnl2.addStyleName(MyResource.myResource.getUISaludarCss().pnl());
		pnl3.addStyleName(MyResource.myResource.getUISaludarCss().pnl());
		txtNombre.getElement().setId("idtxt");
		/*pnl1.addStyleName("pnl");
		pnl2.addStyleName("pnl");
		pnl3.addStyleName("pnl");
		pnl3.getElement().setClassName("pnl");
		txtNombre.getElement().setId("idtxt");*/
	}
}
