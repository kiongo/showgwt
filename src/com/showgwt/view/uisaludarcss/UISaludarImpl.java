package com.showgwt.view.uisaludarcss;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.showgwt.client.service.ServiceSaludar;
import com.showgwt.client.service.ServiceSaludarAsync;

public class UISaludarImpl extends UISaludar{
	private final ServiceSaludarAsync service=GWT.create(ServiceSaludar.class);
	
	@Override
	public void saludar() {
		// TODO Auto-generated method stub
		service.getSaludar(this.txtNombre.getText(), new AsyncCallback<String>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.alert(caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				Window.alert(result);
			}});
	}
	
}
