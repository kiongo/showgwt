package com.showgwt.view.uisaludarcss;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;;

public class UISaludar extends Composite implements UISaludarInter,ClickHandler{
	private FlowPanel pnlContenedor;
	private FlowPanel pnl1;
	private FlowPanel pnl2;
	private FlowPanel pnl3;
	private Label lblTitulo;
	protected TextBox txtNombre;
	private Button btnSaludar;
	
	public UISaludar(){
		initComponents();
		style();
		listenerWidget();
	}
	
	private void initComponents(){
		pnlContenedor=new FlowPanel();
		pnl1=new FlowPanel();
		pnl2=new FlowPanel();
		pnl3=new FlowPanel();
		lblTitulo=new Label("Quien eres: ");
		txtNombre=new TextBox();
		btnSaludar=new Button("Saludar");
		pnl1.add(lblTitulo);
		pnl2.add(txtNombre);
		pnl3.add(btnSaludar);
		pnlContenedor.add(pnl1);
		pnlContenedor.add(pnl2);
		pnlContenedor.add(pnl3);
		initWidget(pnlContenedor);
	}
	
	private void style(){
		pnl1.addStyleName("pnl");
		pnl2.addStyleName("pnl");
		//pnl3.addStyleName("pnl");
		pnl3.getElement().setClassName("pnl");
		txtNombre.getElement().setId("idtxt");
	}
	
	private void listenerWidget(){
		btnSaludar.addClickHandler(this);
	}

	@Override
	public void saludar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(btnSaludar)){
			saludar();
		}
	}
}
