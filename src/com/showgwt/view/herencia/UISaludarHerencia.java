package com.showgwt.view.herencia;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

public class UISaludarHerencia extends FlowPanel implements ClickHandler{	
	private Label lblNombre;
	private TextBox txtNombre;
	private Button btnSaludar;
	
	public UISaludarHerencia(){
		initComponents();
		widgetListener();
	}
	
	private void initComponents(){		
		lblNombre=new Label("Quien eres");
		txtNombre=new TextBox();
		btnSaludar=new Button("Saludar");
		this.add(lblNombre);
		this.add(txtNombre);
		this.add(btnSaludar);		
	}
	
	private void widgetListener(){
		btnSaludar.addClickHandler(this);
	}
	
	private void saludar(){
		Window.alert("Hola Herencia "+txtNombre.getText());
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(btnSaludar)){
			saludar();
		}
	}
	
}
