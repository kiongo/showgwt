package com.showgwt.view.bean;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

public class UISaludarBean implements ClickHandler{
	private FlowPanel pnlContenedor;
	private Label lblNombre;
	private TextBox txtNombre;
	private Button btnSaludar;
	
	public UISaludarBean(){
		initComponents();
		widgetListener();
	}
	
	private void initComponents(){	
		pnlContenedor=new FlowPanel();
		lblNombre=new Label("Quien eres");
		txtNombre=new TextBox();
		btnSaludar=new Button("Saludar");
		pnlContenedor.add(lblNombre);
		pnlContenedor.add(txtNombre);
		pnlContenedor.add(btnSaludar);		
	}
	
	private void widgetListener(){
		btnSaludar.addClickHandler(this);
	}
	
	private void saludar(){
		Window.alert("Hola Bean "+txtNombre.getText());
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(btnSaludar)){
			saludar();
		}		
	}

	public FlowPanel getPnlContenedor() {
		return pnlContenedor;
	}
	
	
	
}
