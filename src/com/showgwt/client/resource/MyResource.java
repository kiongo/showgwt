package com.showgwt.client.resource;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.showgwt.client.resource.cssresource.UISaludarCss;

public interface MyResource extends ClientBundle{
	public static final MyResource myResource=GWT.create(MyResource.class);
	
	@Source("style/uisaludarcss.css")
	UISaludarCss getUISaludarCss();

}
