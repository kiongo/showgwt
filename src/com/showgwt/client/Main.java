package com.showgwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

public class Main implements EntryPoint,ClickHandler{
	private Label lblNombre;
	private TextBox txtNombre;
	private Button btnEnviar;
	private FlowPanel panel;
	
	@Override
	public void onModuleLoad() {
		// TODO Auto-generated method stub
		lblNombre=new Label("Indicar Nombre");
		txtNombre=new TextBox();
		btnEnviar=new Button("Saludar");
		panel=new FlowPanel();
		btnEnviar.addClickHandler(this);
		panel.add(lblNombre);
		panel.add(txtNombre);
		panel.add(btnEnviar);
		RootPanel.get().add(panel);
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		Window.alert("Hola "+txtNombre.getText());
	}

}
