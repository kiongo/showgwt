package com.showgwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.showgwt.view.bean.UISaludarBean;
import com.showgwt.view.composite.UISaludarComposite;
import com.showgwt.view.herencia.UISaludarHerencia;

public class MainTresFormas implements EntryPoint{
	private UISaludarComposite uiSaludarComposite;
	private UISaludarHerencia uiSaludarHerencia;
	private UISaludarBean uiSaludarBean;

	@Override
	public void onModuleLoad() {
		// TODO Auto-generated method stub
		uiSaludarComposite=new UISaludarComposite();
		uiSaludarHerencia=new UISaludarHerencia();
		uiSaludarBean=new UISaludarBean();
		RootPanel.get("uiformacomposite").add(uiSaludarComposite);
		RootPanel.get("uiformaherencia").add(uiSaludarHerencia);
		RootPanel.get("uiformabean").add(uiSaludarBean.getPnlContenedor());
	}

}
