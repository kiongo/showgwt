package com.showgwt.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ServiceSaludarAsync {

	void getSaludar(String param, AsyncCallback<String> callback);

}
