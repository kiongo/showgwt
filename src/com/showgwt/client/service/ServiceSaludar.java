package com.showgwt.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("servicesaludar")
public interface ServiceSaludar extends RemoteService{
	String getSaludar(String param);
}
