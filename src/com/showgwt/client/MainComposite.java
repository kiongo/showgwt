package com.showgwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.showgwt.view.composite.UISaludarComposite;

public class MainComposite implements EntryPoint{
	private UISaludarComposite uiSaludar;

	@Override
	public void onModuleLoad() {
		// TODO Auto-generated method stub
		uiSaludar=new UISaludarComposite();
		RootPanel.get().add(uiSaludar);
	}

}
