package com.showgwt.server.rpc;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.showgwt.client.service.ServiceSaludar;

public class ServiceSaludarImpl extends RemoteServiceServlet implements ServiceSaludar{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7040718007749714780L;

	@Override
	public String getSaludar(String param) {
		// TODO Auto-generated method stub
		return "Hola "+param+" Bienvenido a la comunicacion con el Servidor a traves de RPC";
	}

}
